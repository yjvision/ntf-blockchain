// App object
App = {
    //object member variables
    web3Provider: null,//store web3 reference
    contracts: {},//instantiate an empty object
    /*
    init :  1.Setup web3 provider,
            2.Load contracts,
            3,Binds events to html components
    */
    init: ()=>{
         // set the provider you want from Web3.providers
    App.web3Provider = new Web3.providers.HttpProvider('http://127.0.0.1:7545');
    web3 = new Web3(App.web3Provider);
    //Next, we load the contracts
    return App.initContract();
    },
    initContract: () => {
        $.getJSON('Creature.json', (data) => {
            // Get the necessary contract artifact file and instantiate it with truffle-contract.
            var MyTokenArtifact = data;
            App.contracts.MyToken = TruffleContract(MyTokenArtifact);
       
            // Set the provider for our contract.
            App.contracts.MyToken.setProvider(App.web3Provider);
       
            // Use our contract to retrieve and mark the adopted pets.
            return App.getAccountDetails();
          });
          //Next, we bind the event handlers of html components
          return App.bindEvents();
        },
       
        getAccountDetails: async () => {
            /*Retrieve all the accounts that is currently connected
          to the blockchain*/
            web3.eth.getAccounts(async (error, accounts) => {
              if (error) console.log(error);
              //Use the first account
              App.account = accounts[0];
              //Display the wallet address in the place holder
              $('#MyTokenWallet').text(App.account)
              //Get the deployed contract
              myTokenInstance = await App.contracts.MyToken.deployed();
              console.log(myTokenInstance);
              // Get Account Balance
              result = await myTokenInstance.balanceOf(App.account);
              balance = result.toNumber();
              console.log(result);
              // Display number of token
              $('#MyTokenBalance').text(balance);
              // Get 3 letters symbol
              let symbol = await myTokenInstance.symbol();
              // Display symbol, example "TKH"
              $('#MyTokenSymbol').text(symbol);
              // Get Total Supply of this NFT
              let totalSupply = await myTokenInstance.totalSupply();
              // Set isOwner to false by default
              let isOwner = false;
              // Declare a string variable
              var membership_html = ""
              // Loop through the total supply of this NFT to check
              // any of the NFT owned by this account
              for(let i=1; i <= totalSupply.toNumber(); i++){
                console.log("checking --- ")
                // Get current Owner account
                var owner = await myTokenInstance.ownerOf(i);
                // If owner of NFT matched, create a card and append to the membership_html
                if(owner==App.account){
                  var tokenuri = await myTokenInstance.tokenURI(i);
                  var response = await fetch(tokenuri)
                  var data = await response.json();
                  //console.log(data);
                  membership_html += '<div class="card" style="margin: 20px;"><img id="MyTokenImage" class="card-img-top mx-auto d-block" src="';
                  membership_html += data.image;
                  membership_html +='" alt="Card image" style="width: 60%;"><div class="card-body" style="text-align:center;"><h4 class="card-title" id="tid">';
                  membership_html += '#' + i + '</h4></div></div>';
                  // Set the current account owns a NFT
                  isOwner = true;          
                }
              }
              // If current account owns a NFT, display the membership_html
              if(isOwner){
                $('#membership').html(membership_html);
              }else{ // else display the following message
                $('#membership').html("Sorry, no membership found.");
              }
            })
          },      
    }
// Web page loaded event handler
$(() => {
  $(window).load(()=>{
    App.init();
  });
});
