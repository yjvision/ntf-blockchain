// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;
import "./ERC721Tradable.sol";
/**
 * @title Creature
 * Creature - a contract for my non-fungible creatures.
 */
contract Creature is ERC721Tradable {
    constructor(address _proxyRegistryAddress)
        ERC721Tradable("GCIT_Token", "GCIT", _proxyRegistryAddress)
    {}
   function baseTokenURI() override public pure returns (string memory) {
        return "https://api.npoint.io/06a5c775c0faec4f9b7f";
    }
 
    function contractURI() public pure returns (string memory) {
        return "https://api.npoint.io/349947a149bc14d7cd99";
    }
}
